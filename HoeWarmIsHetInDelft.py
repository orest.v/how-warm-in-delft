import logging
import sys

import requests


def get_temperature_in_delft() -> float:
    """Fetch the temperature from weerindelft.nl.

    Reuse endpoint used by the page's AJAX.
    https://weerindelft.nl/clientraw.txt returns a space-separated string of numbers,
    the fifth of them is the current temperature in Celsius.
    Obtain the temperature and ignore the rest.
    TODO: raise meaningful exceptions if the format changes.
    """
    response = requests.get('https://weerindelft.nl/clientraw.txt')
    return float(response.text.split()[4])


def format_output(temperature: float):
    return f'{temperature:.0f} degrees Celsius'


def setup_logging():
    root = logging.getLogger()
    root.setLevel(logging.INFO)
    hdlr = logging.StreamHandler(sys.stderr)
    hdlr.setLevel(logging.INFO)
    root.addHandler(hdlr)


def main():
    setup_logging()
    try:
        print_temperature_in_delft(sys.stdout)
    except Exception as e:
        logging.exception(e)
        exit(1)
    except KeyboardInterrupt:
        logging.warning('Process interrupted')
        exit(1)


def print_temperature_in_delft(stream):
    temperature = get_temperature_in_delft()
    formatted_temperature = format_output(temperature)
    stream.write(formatted_temperature)


if __name__ == "__main__":
    main()
