FROM python:3.7

WORKDIR /root/howwarm

ADD HoeWarmIsHetInDelft.py .
ADD requirements.txt .

RUN pip install -r requirements.txt

ENTRYPOINT python HoeWarmIsHetInDelft.py
